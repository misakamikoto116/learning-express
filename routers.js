const express = require('express')
const { ObjectId } = require('mongodb')
const router = express.Router()

require('./mongoose')
const User = require('./User')

router.get('/', function(req, res) {
  res.send('Hello bang')
})

router.get('/users', async function(req, res) {
  try {
    const users = await User.find()

    res.send({data: users})
  } catch (error) {
    res.send({message: err.message || 'Internal server error'})
  }
})

router.get('/', function(req, res) {
  res.send('Hello bang')
})

module.exports = router;