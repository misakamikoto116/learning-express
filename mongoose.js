const mongoose = require('mongoose')

const uri = 'mongodb://127.0.0.1:27017/admin'
mongoose.connect(uri)

const db = mongoose.connection
db.on('error', console.error.bind(console, 'Connection error:'))
db.once('open', () => console.log("Server berhasil konek"))